
public final class Fraction {
	private final int numerateur;
	private final int denominateur;
	
	public Fraction() {
		this.numerateur = 0;
		this.denominateur = 1;
	} 
	public Fraction(int numerateur, int denominateur) {
		this.denominateur = denominateur;
		this.numerateur = numerateur;
	}
	public Fraction(int numerateur) {
		this.numerateur = numerateur;
		this.denominateur = 1;
	}
	public int getNumerateur() {
		return this.numerateur;
	}
	public int getDenominateur() {
		return this.denominateur;
	}
	public float getResultat() {
		return (float)((float)this.numerateur/(float)this.denominateur);
	}
	@Override
	public String toString() {
		return this.getNumerateur() + " / " + this.getDenominateur() + " = " + 
				this.getResultat();
	}
}
