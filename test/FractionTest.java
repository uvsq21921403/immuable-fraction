import static org.junit.Assert.*;

import org.junit.Test;

public class FractionTest {

	@Test
	public void testFraction() {
		Fraction fract= new Fraction();
		assertEquals(0,fract.getNumerateur());
		assertEquals(1,fract.getDenominateur());
	}
	
	@Test 
	public void testFractionUnArg() { 		
	Fraction fract= new Fraction(41);
	assertEquals(1,fract.getDenominateur()); }
	
	@Test 
	public void testFractionDeuxArg() 
	{ 		
	Fraction fract= new Fraction(0,1);
	assertEquals(0,fract.getNumerateur());
	assertEquals(1,fract.getDenominateur()); }	

	@Test 
	public void testgetNumerateur1() {
	Fraction f=new Fraction(20);
	assertEquals(20, f.getNumerateur());	
	}
	
	@Test 
	public void testgetNumerateur2() {
	Fraction f=new Fraction(0,1);
	assertEquals(0, f.getNumerateur());	
	}
	
	@Test 
	public void testgetNumerateur3() {
	Fraction f=new Fraction();
	assertEquals(0, f.getNumerateur());	
	}
	
	@Test 
	public void testgetDenominateur1() {
	Fraction f=new Fraction(20);
	assertEquals(1, f.getDenominateur());	
	}
	
	@Test 
	public void testgetDenominateur2() {
	Fraction f=new Fraction();
	assertEquals(1, f.getDenominateur());	
	}
	
	@Test 
	public void testgetDenominateur3() {
	Fraction f=new Fraction(0,1);
	assertEquals(1, f.getDenominateur());	
	}

	@Test 
	public void testgetResultat() {
	Fraction f=new Fraction(5,2);
	assertEquals(2.5f, f.getResultat(),0.0000001f);
	}

}

